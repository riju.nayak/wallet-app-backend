package com.pathashala.walletservice.transaction;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    @Query("SELECT COALESCE(SUM(amount),0) FROM Transaction where createdon>=CURRENT_DATE AND wallet_id=:wallet_id")
    int findTransactionByCurrentDay(@Param("wallet_id") long wallet_id);

    List<Transaction> findAllByWalletId(long wallet_id, Pageable pageable);
}
