package com.pathashala.walletservice.transaction;

import com.pathashala.walletservice.service.TransactionService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
public class TransactionController {
    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionService transactionService;

    public TransactionController(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    @PostMapping(value = "/wallets/{walletId}/transactions")
    @ResponseStatus(HttpStatus.CREATED)
    Transaction postTransaction(@PathVariable long walletId, @RequestBody Transaction transaction) {
        transactionService.performTransaction(transaction, walletId);
        return transaction;
    }

    @GetMapping(value = "/wallets/{walletId}/transactions")
    @ResponseStatus(HttpStatus.OK)
    List<Transaction> getTransactions(@PathVariable long walletId,
                                      @RequestParam(defaultValue = "1") int pageNumber,
                                      @RequestParam(defaultValue = "10") int pageSize) {
        return transactionService.findAllTransactions(walletId, pageNumber, pageSize);
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST,reason = "Invalid Data")
    @ExceptionHandler({HttpMessageNotReadableException.class, HibernateException.class})
    public void handleGeneralException(@RequestBody Exception exception) {
    }
}

