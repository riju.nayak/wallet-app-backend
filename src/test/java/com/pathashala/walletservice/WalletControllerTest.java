package com.pathashala.walletservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pathashala.walletservice.wallet.Wallet;
import com.pathashala.walletservice.wallet.WalletController;
import com.pathashala.walletservice.wallet.WalletRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WalletController.class)
class WalletControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WalletRepository walletRepository;

    @Test
    void shouldGetAllWallets() throws Exception {
        List<Wallet> wallets = Arrays.asList(
                new Wallet(1L, 100, "Walter White"),
                new Wallet(2L, 200, "Saul Godman")
        );
        when(walletRepository.findAll()).thenReturn(wallets);

        mockMvc.perform(get("/wallets")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].balance").value("100"))
                .andExpect(jsonPath("$[1].balance").value("200"));
    }

    @Test
    void shouldPostWallet() throws Exception {
        Wallet wallet = new Wallet(100, "Jon");
        when(walletRepository.save(any(Wallet.class))).thenReturn(wallet);

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/wallets")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(wallet)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").exists());

        verify(walletRepository).save(any(Wallet.class));
    }

    @Test
    void shouldGetAWallet() throws Exception {
        Optional<Wallet> wallet = Optional.of(new Wallet(1L, 100, "John"));
        when(walletRepository.findById(any(Long.class))).thenReturn(wallet);

        mockMvc.perform(get("/wallets/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance").value(100))
                .andExpect(jsonPath("$.name").value("John"))
                .andExpect(jsonPath("$.id").value(1));
    }
}
