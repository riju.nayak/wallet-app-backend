package com.pathashala.walletservice.transaction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pathashala.walletservice.wallet.Wallet;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int amount;

    public String getRemarks() {
        return remarks;
    }

    private String remarks;

    public long getId() {
        return id;
    }

    public Timestamp getCreatedon() {
        return createdon;
    }

    @CreationTimestamp
    private Timestamp createdon;

    public void setTransactiontype(TransactionType transactiontype) {
        this.transactiontype = transactiontype;
    }

    @Enumerated(EnumType.STRING)
    private TransactionType transactiontype;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "wallet_id")
    private Wallet wallet;

    public Transaction() {
    }

    public Transaction(int amount, TransactionType transactionType) {

        this.amount = amount;
        this.transactiontype = transactionType;
        this.createdon = new Timestamp(new Date().getTime());
    }

    public int getAmount() {
        return amount;
    }

    public void linkWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public TransactionType getTransactiontype() {
        return transactiontype;
    }
}