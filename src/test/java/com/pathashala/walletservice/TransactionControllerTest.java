package com.pathashala.walletservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pathashala.walletservice.exceptions.InvalidTransactionException;
import com.pathashala.walletservice.service.TransactionService;
import com.pathashala.walletservice.transaction.Transaction;
import com.pathashala.walletservice.transaction.TransactionController;
import com.pathashala.walletservice.transaction.TransactionRepository;
import com.pathashala.walletservice.transaction.TransactionType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TransactionController.class)
class TransactionControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TransactionRepository transactionRepository;

    @MockBean
    private TransactionService transactionService;

    @Test
    void shouldBeAbleCreate() throws Exception {
        Transaction transaction = new Transaction(100, TransactionType.CREDIT);

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/wallets/2/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(transaction)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("amount").value("100"));

    }

    @Test
    void shouldBeAbleToDepositWallet() throws Exception {
        Transaction transaction = new Transaction(10, TransactionType.CREDIT);

        when(transactionRepository.save(any(Transaction.class))).thenReturn(transaction);

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/wallets/1/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(transaction)))
                .andExpect(status().isCreated());
    }

    @Test
    void shouldBeAbleToGetAllTransaction() throws Exception {
        Transaction transaction = new Transaction(10, TransactionType.CREDIT);
        when(transactionService.findAllTransactions(1, 1, 7)).thenReturn(Arrays.asList(transaction));

        mockMvc.perform(get("/wallets/1/transactions?pageNumber=1&pageSize=7")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].amount").value(10));

        verify(transactionService).findAllTransactions(1, 1, 7);

    }

    @Test
    void shouldSendNotAcceptableStatusWhenTransactionHasInvalidAmount() throws Exception {
        int invalidTransactionAmount = 50001;
        Transaction transaction = new Transaction(invalidTransactionAmount, TransactionType.CREDIT);

        doThrow(InvalidTransactionException.class).when(transactionService).performTransaction(any(Transaction.class), any(Long.class));

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/wallets/1/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(transaction)))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    void shouldGiveBadRequestWhenInvalidTransactionData() throws Exception {
        String invalidAmountTransaction = "{\n" +
                "\t\"amount\":\"2e1\",\n" +
                "\t\"remarks\":\"sa\",\n" +
                "\t\"transactiontype\":\"CREDIT\"\n" +
                "}";


        mockMvc.perform(post("/wallets/1/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(invalidAmountTransaction))
                .andExpect(status().isBadRequest());

    }
}
