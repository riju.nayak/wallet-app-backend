package com.pathashala.walletservice;

import com.pathashala.walletservice.exceptions.InvalidTransactionException;
import com.pathashala.walletservice.exceptions.WalletNotFoundException;
import com.pathashala.walletservice.service.TransactionService;
import com.pathashala.walletservice.transaction.Transaction;
import com.pathashala.walletservice.transaction.TransactionRepository;
import com.pathashala.walletservice.transaction.TransactionType;
import com.pathashala.walletservice.wallet.Wallet;
import com.pathashala.walletservice.wallet.WalletRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TransactionServiceTest {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Test
    void shouldCreditWalletIfWalletPresent() throws WalletNotFoundException, InvalidTransactionException {
        Wallet wallet = new Wallet(100, "Jon Don");
        Wallet saveWallet = walletRepository.save(wallet);
        Transaction transaction = new Transaction(100, TransactionType.CREDIT);

        transactionService.performTransaction(transaction, saveWallet.getId());
        transactionService.performTransaction(new Transaction(100, TransactionType.CREDIT), saveWallet.getId());
        Wallet updatedWallet = walletRepository.findById(saveWallet.getId()).get();

        assertEquals(300, updatedWallet.getBalance());
        assertEquals(2, transactionRepository.count());
    }

    @Test
    void shouldNotCreditWalletIfWalletIsNotPresent() {
        Transaction transaction = new Transaction(100, TransactionType.CREDIT);

        assertThrows(WalletNotFoundException.class,()->transactionService.performTransaction(transaction, 20L));
    }

    @Test
    void shouldBeAbleToGetAllTransaction() throws Exception {
        transactionService.findAllTransactions(1, 1, 7);
        List<Transaction> list=transactionRepository.findAll();

        assertNotNull(list);
    }

    @Test
    void shouldBeaBleToFindTheTotalTransactionForADay() {
        Wallet wallet = new Wallet(100, "Jon Don");
        Wallet saveWallet = walletRepository.save(wallet);
        Transaction transaction = new Transaction(78, TransactionType.CREDIT);

        transactionService.performTransaction(transaction, saveWallet.getId());
        transactionService.performTransaction(new Transaction(100, TransactionType.CREDIT), saveWallet.getId());
        int total = transactionRepository.findTransactionByCurrentDay(saveWallet.getId());

        assertEquals(total,178);
    }

    @Test
    void shouldNotBeAbleToDepositAbove50000LimitPerDay() {
        Wallet wallet = new Wallet(100, "Jon Don");
        Wallet saveWallet = walletRepository.save(wallet);
        Transaction transaction = new Transaction(49990, TransactionType.CREDIT);
        transactionService.performTransaction(transaction, saveWallet.getId());

        Transaction invalidTransaction = new Transaction(11, TransactionType.CREDIT);

        assertThrows(InvalidTransactionException.class,()->
                transactionService.performTransaction(invalidTransaction, saveWallet.getId()));
    }

    @Test
    void shouldNotBeAbleToDepositAmountBelow10(){
        Wallet wallet = new Wallet(100, "Jon Don");
        Wallet saveWallet = walletRepository.save(wallet);

        Transaction invalidTransaction = new Transaction(9, TransactionType.CREDIT);

        assertThrows(InvalidTransactionException.class,()->
                transactionService.performTransaction(invalidTransaction, saveWallet.getId()));
    }

    @Test
    void shouldBeAbleToWithdrawFromWallet() {
        Wallet wallet = new Wallet(100, "Jon Don");
        Wallet saveWallet = walletRepository.save(wallet);

        Transaction transaction = new Transaction(20, TransactionType.DEBIT);

        assertDoesNotThrow(()->transactionService.performTransaction(transaction, saveWallet.getId()));

        Optional<Wallet> afterWithdrawWallet = walletRepository.findById(saveWallet.getId());
        Wallet updatedWallet = afterWithdrawWallet.get();

        assertEquals(80,updatedWallet.getBalance());
    }
    @Test
    void shouldNotBeAbleToWithdrawMoreThanCurrentBalance() {
        Wallet wallet = new Wallet(100, "Jon Don");
        Wallet saveWallet = walletRepository.save(wallet);

        Transaction transaction = new Transaction(101, TransactionType.DEBIT);

        assertThrows(InvalidTransactionException.class, () ->
                transactionService.performTransaction(transaction, saveWallet.getId()));
    }

    @Test
    void shouldShowSevenRecentTransactionWhenMoreThanSeven(){
        Wallet wallet = new Wallet(100, "Jon Don");
        Wallet saveWallet = walletRepository.save(wallet);

        Transaction firstTransaction = new Transaction(10, TransactionType.CREDIT);
        Transaction secondTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction thirdTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction fourthTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction fifthTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction sixthTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction seventhTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction eighthTransaction = new Transaction(19, TransactionType.CREDIT);

        transactionService.performTransaction(firstTransaction, saveWallet.getId());
        transactionService.performTransaction(secondTransaction, saveWallet.getId());
        transactionService.performTransaction(thirdTransaction, saveWallet.getId());
        transactionService.performTransaction(fourthTransaction, saveWallet.getId());
        transactionService.performTransaction(fifthTransaction, saveWallet.getId());
        transactionService.performTransaction(sixthTransaction, saveWallet.getId());
        transactionService.performTransaction(seventhTransaction, saveWallet.getId());
        transactionService.performTransaction(eighthTransaction, saveWallet.getId());

        assertEquals(7, transactionService.findAllTransactions(saveWallet.getId(), 1, 7).size());

    }

    @Test
    void shouldShowAllRecentTransactionWhenLessThanSeven(){
        Wallet wallet = new Wallet(100, "Jon Don");
        Wallet saveWallet = walletRepository.save(wallet);

        Transaction firstTransaction = new Transaction(10, TransactionType.CREDIT);
        Transaction secondTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction thirdTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction fourthTransaction = new Transaction(19, TransactionType.CREDIT);

        transactionService.performTransaction(firstTransaction, saveWallet.getId());
        transactionService.performTransaction(secondTransaction, saveWallet.getId());
        transactionService.performTransaction(thirdTransaction, saveWallet.getId());
        transactionService.performTransaction(fourthTransaction, saveWallet.getId());

        assertEquals(4, transactionService.findAllTransactions(saveWallet.getId(), 1, 7).size());

    }

    @Test
    void shouldOnlyShowRecentTransactionForGivenWalletId(){
        Wallet wallet = new Wallet(100, "Jon Don");
        Wallet saveWallet = walletRepository.save(wallet);

        Wallet anotherWallet = new Wallet(100, "Jon Don");
        Wallet anotherSaveWallet = walletRepository.save(anotherWallet);

        Transaction firstTransaction = new Transaction(10, TransactionType.CREDIT);
        Transaction secondTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction thirdTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction fourthTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction fifthTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction sixthTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction seventhTransaction = new Transaction(19, TransactionType.CREDIT);
        Transaction eighthTransaction = new Transaction(19, TransactionType.CREDIT);

        transactionService.performTransaction(firstTransaction, saveWallet.getId());
        transactionService.performTransaction(secondTransaction, saveWallet.getId());
        transactionService.performTransaction(thirdTransaction, saveWallet.getId());
        transactionService.performTransaction(fourthTransaction, saveWallet.getId());
        transactionService.performTransaction(fifthTransaction, saveWallet.getId());
        transactionService.performTransaction(sixthTransaction, anotherSaveWallet.getId());
        transactionService.performTransaction(seventhTransaction, anotherSaveWallet.getId());
        transactionService.performTransaction(eighthTransaction, anotherSaveWallet.getId());

        assertEquals(5, transactionService.findAllTransactions(saveWallet.getId(), 1, 7).size());
    }

}
