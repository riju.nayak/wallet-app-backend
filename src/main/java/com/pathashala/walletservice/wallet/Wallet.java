package com.pathashala.walletservice.wallet;

import com.pathashala.walletservice.transaction.Transaction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

@Entity
public class Wallet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int balance;
    private String name;

    @OneToMany(
            mappedBy = "wallet",
            cascade = ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    private List<Transaction> transactions = new ArrayList<>();

    public Wallet() {
    }

    public Wallet(long id, int balance, String name) {
        this.id = id;
        this.balance = balance;
        this.name = name;
    }

    public Wallet(int balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public int getBalance() {
        return balance;
    }

    public void credit(Transaction transaction) {
        balance += transaction.getAmount();
        addTransactionToWallet(transaction);
    }


    public void debit(Transaction transaction) {
        balance -= transaction.getAmount();
        addTransactionToWallet(transaction);
    }

    private void addTransactionToWallet(Transaction transaction) {
        transaction.linkWallet(this);
        transactions.add(transaction);
    }

    public String getName() {
        return name;
    }
}
