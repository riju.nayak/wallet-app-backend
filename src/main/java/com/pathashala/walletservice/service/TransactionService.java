package com.pathashala.walletservice.service;

import com.pathashala.walletservice.exceptions.InvalidTransactionException;
import com.pathashala.walletservice.exceptions.WalletNotFoundException;
import com.pathashala.walletservice.transaction.Transaction;
import com.pathashala.walletservice.transaction.TransactionRepository;
import com.pathashala.walletservice.transaction.TransactionType;
import com.pathashala.walletservice.wallet.Wallet;
import com.pathashala.walletservice.wallet.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionService {
    private static final String DAILY_LIMIT_EXCEEDED_MESSAGE = "Daily Limit Exceeded";
    private static final int MINIMUM_TRANSACTION_AMOUNT = 10;
    private static final String MINIMUM_AMOUNT_ALLOWED_MESSAGE = "Minimum amount allowed is " + MINIMUM_TRANSACTION_AMOUNT;
    private static final int TOTAL_TRANSACTION_AMOUNT_PER_DAY_THRESHOLD = 50000;
    private static final String LOW_BALANCE_MESSAGE = "Insufficient Balance";


    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private WalletRepository walletRepository;

    public void performTransaction(Transaction transaction, long walletId) {
        Wallet wallet = walletRepository
                .findById(walletId)
                .orElseThrow(WalletNotFoundException::new);
        validateTransaction(transaction, wallet);

        if (transaction.getTransactiontype() == TransactionType.CREDIT) {
            wallet.credit(transaction);
        } else {
            wallet.debit(transaction);
        }

        walletRepository.save(wallet);
    }

    private void validateTransaction(Transaction transaction, Wallet wallet) {
        int todayTotalTransactionsAmountBefore = transactionRepository.findTransactionByCurrentDay(wallet.getId());
        int todayTotalTransactionsAmountAfter = todayTotalTransactionsAmountBefore + transaction.getAmount();

        if (todayTotalTransactionsAmountAfter >= TOTAL_TRANSACTION_AMOUNT_PER_DAY_THRESHOLD) {
            throw new InvalidTransactionException(DAILY_LIMIT_EXCEEDED_MESSAGE);
        }
        if (transaction.getAmount() < MINIMUM_TRANSACTION_AMOUNT) {
            throw new InvalidTransactionException(MINIMUM_AMOUNT_ALLOWED_MESSAGE);
        }
        if (transaction.getTransactiontype() == TransactionType.DEBIT &&
                transaction.getAmount() > wallet.getBalance()){
            throw new InvalidTransactionException(LOW_BALANCE_MESSAGE);
        }
    }

    public List<Transaction> findAllTransactions(long wallet_id, int pageNumber, int pageSize) {
        Sort sort = Sort.by(Sort.Direction.DESC, "createdon");
        Pageable pagination = PageRequest.of(pageNumber - 1, pageSize, sort);

        List<Transaction> transactions = transactionRepository
                                            .findAllByWalletId(wallet_id, pagination);
        return transactions;
    }
}
