package com.pathashala.walletservice.wallet;

import com.pathashala.walletservice.service.TransactionService;
import com.pathashala.walletservice.transaction.Transaction;
import com.pathashala.walletservice.transaction.TransactionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.lang.model.util.Elements;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/wallets")
public class WalletController {
    @Autowired
    private WalletRepository walletRepository;

    public WalletController(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    @GetMapping
    List<Wallet> getWalletDetails(){
        return walletRepository.findAll();
    }

    @GetMapping("/{id}")
    Wallet getWallet(@PathVariable Long id){
        Wallet wallet = walletRepository.findById(id).get();
        return wallet;
    }

    @PostMapping
    Wallet addWallet(@RequestBody Wallet wallet){
        return walletRepository.save(wallet);
    }


}
