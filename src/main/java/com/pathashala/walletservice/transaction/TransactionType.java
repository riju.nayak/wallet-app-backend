package com.pathashala.walletservice.transaction;

public enum TransactionType {
    CREDIT,
    DEBIT
}
